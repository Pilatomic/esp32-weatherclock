#ifndef JSON_DATA_H
#define JSON_DATA_H

#include "freertos/FreeRTOS.h"


#define WEATHER_MAIN_MAX_LEN    13
#define WEATHER_DESC_MAX_LEN    32

struct DataStruct_t {
    int dataValid;
    int cod;
    int dt;
    int weathersCount;
    double temp;
    int timeOffset;
    char mainWeatherText[WEATHER_MAIN_MAX_LEN];
    char descriptionWeatherText[WEATHER_DESC_MAX_LEN];
};
typedef struct DataStruct_t DataStruct_t;


void data_init();

int data_takeMutex(TickType_t ticksToWait);
int data_giveMutex();

int data_parseBuffer(char* ptr, int len);

DataStruct_t* data_getDataStruct();

#endif
