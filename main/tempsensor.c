#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "esp_system.h"
#include "esp_err.h"
#include "esp_log.h"
#include "driver/gpio.h"

#include "tempsensor.h"

#define DS18B20_VCC_PIN 18
#define DS18B20_DQ_PIN  19

#define TAG "TEMPSENSOR"

#define DS18B20_SCRATCHPAD_SIZE     9
#define DS18B20_CMD_SKIPROM         0xCC
#define DS18B20_CMD_CONVERT         0x44
#define DS18B20_CMD_READ_SCRATCHPAD 0xBE

static int tempValue = TEMPSENSOR_ERROR;
static SemaphoreHandle_t tempMutex = NULL;

static void config_gpio()
{
    gpio_config_t io_conf;

    //Set VCC Pin
    memset(&io_conf, 0, sizeof(gpio_config_t));
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = 1ULL << DS18B20_VCC_PIN;
    ESP_ERROR_CHECK(gpio_config(&io_conf));
    gpio_set_level(DS18B20_VCC_PIN, 1);

    //Set VCC Pin
    memset(&io_conf, 0, sizeof(gpio_config_t));
    io_conf.mode = GPIO_MODE_INPUT_OUTPUT_OD;
    io_conf.pull_up_en = GPIO_PULLDOWN_ENABLE;
    io_conf.pin_bit_mask = 1ULL << DS18B20_DQ_PIN;
    ESP_ERROR_CHECK(gpio_config(&io_conf));
}

static int ds18b20_reset_and_check_present()
{
    int ok = 1;

    gpio_set_level(DS18B20_DQ_PIN,0);
    ets_delay_us(500);
    gpio_set_level(DS18B20_DQ_PIN,1);
    ets_delay_us(90);
    if(gpio_get_level(DS18B20_DQ_PIN) != 0) ok = 0;
    ets_delay_us(300);
    if(gpio_get_level(DS18B20_DQ_PIN) != 1) ok = 0;

    return ok;
}

static void ds18b20_write_byte(uint8_t byte)
{
    for(int i = 0 ; i < 8 ; i++)
    {
        gpio_set_level(DS18B20_DQ_PIN, 0);
        ets_delay_us(2);
        gpio_set_level(DS18B20_DQ_PIN, byte & 0x01);
        ets_delay_us(80);
        gpio_set_level(DS18B20_DQ_PIN, 1);
        ets_delay_us(18);
        byte >>= 1;
    }
}

static uint8_t ds18b20_read_byte()
{
    int byte = 0;
    for(int i = 0 ; i < 8 ; i++)
    {
        byte >>= 1;
        gpio_set_level(DS18B20_DQ_PIN, 0);
        ets_delay_us(2);
        gpio_set_level(DS18B20_DQ_PIN, 1);
        ets_delay_us(10);
        if(gpio_get_level(DS18B20_DQ_PIN)) byte |= 0x80;
        ets_delay_us(88);
    }
    return byte;
}

static int ds18b20_perform_conversion()
{
    if(!ds18b20_reset_and_check_present()) return 0;
    ds18b20_write_byte(DS18B20_CMD_SKIPROM);
    ds18b20_write_byte(DS18B20_CMD_CONVERT);
    vTaskDelay(1000 / portTICK_PERIOD_MS);
    return 1;
}

static int ds18b20_read_temperature()
{
    int temp = TEMPSENSOR_ERROR;

    if(ds18b20_reset_and_check_present())
    {

        ds18b20_write_byte(DS18B20_CMD_SKIPROM);
        ds18b20_write_byte(DS18B20_CMD_READ_SCRATCHPAD);

        //We only need the first 2 registers
        temp = ds18b20_read_byte();
        temp |= (ds18b20_read_byte() << 8);
        temp >>= 4;
    }
    return temp;
}

void tempsensor_task()
{
    int readTemp;

    tempValue = TEMPSENSOR_ERROR;
    tempMutex = xSemaphoreCreateMutex();

    config_gpio();

    while(1)
    {
        ds18b20_perform_conversion();

        readTemp = ds18b20_read_temperature();
        if(xSemaphoreTake(tempMutex, 1000))
        {
            tempValue = readTemp;
            xSemaphoreGive(tempMutex);
        }

        if(readTemp == TEMPSENSOR_ERROR)
            ESP_LOGE(TAG, "DS18B20 not present !");

        vTaskDelay(5000 / portTICK_PERIOD_MS);
    }
}

int tempsensor_get_temp(TickType_t ticksToWait, int *temp)
{
    if(xSemaphoreTake(tempMutex, ticksToWait))
    {
        *temp = tempValue;
        xSemaphoreGive(tempMutex);
        return 1;
    }
    return 0;
}
