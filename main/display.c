
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_system.h"

#include "hd44780.h"
#include "json_data.h"
#include "tempsensor.h"

#define TAG "DISPLAY"
#define BIG_FONT_CHAR_COUNT 8

static const uint8_t bigFontCustomChars[8][BIG_FONT_CHAR_COUNT] =
{
    { 0b00011, 0b00111, 0b01111, 0b11111, 0b00000, 0b00000, 0b00000, 0b00000 },
    { 0b11000, 0b11100, 0b11110, 0b11111, 0b00000, 0b00000, 0b00000, 0b00000 },
    { 0b11111, 0b11111, 0b11111, 0b11111, 0b00000, 0b00000, 0b00000, 0b00000 },
    { 0b11111, 0b11110, 0b11100, 0b11000, 0b00000, 0b00000, 0b00000, 0b00000 },
    { 0b11111, 0b01111, 0b00111, 0b00011, 0b00000, 0b00000, 0b00000, 0b00000 },
    { 0b00011, 0b00111, 0b01111, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111 },
    { 0b11000, 0b11100, 0b11110, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111 }
};

static const uint8_t bigFontDigits[10][9] =
{
    { 5, 2, 6, 255, 254, 255, 4, 2, 3 },
    { 0, 255, 254, 254, 255, 254, 2, 2, 2 },
    { 0, 2, 6, 5, 2, 3, 4, 2, 3 },
    { 0, 2, 6, 254, 2, 255, 4, 2, 3 },
    { 255, 254, 255, 4, 2, 255, 254, 254, 2 },
    { 255, 2, 3, 2, 2, 6, 4, 2, 3 },
    { 5, 2, 1, 255, 2, 6, 4, 2, 3 },
    { 4, 2, 255, 254, 5, 3, 254, 2, 254 },
    { 5, 2, 6, 255, 2, 255, 4, 2, 3 },
    { 5, 2, 6, 4, 2, 255, 4, 2, 3}
};

static const hd44780_t lcd =
{
    .pins.rs = 33, .pins.e = 25,
    .pins.d4 = 26, .pins.d5 = 27, .pins.d6 = 14, .pins.d7 = 12,
    .font = HD44780_FONT_5X8, .lines = 4
};

static void writeBigFontDigit(int x, int y, int digit)
{
    const uint8_t* charsPtr = bigFontDigits[digit];
    for(int i = 0 ; i < 3 ; i++)
    {
        hd44780_gotoxy(&lcd, x, y + i);
        hd44780_putc(&lcd, *charsPtr++);
        hd44780_putc(&lcd, *charsPtr++);
        hd44780_putc(&lcd, *charsPtr++);
    }
}

static void display_large_clock(int x, int y, time_t *time)
{
    struct tm timeinfo;

    localtime_r(time, &timeinfo);

    //Display time with bigfont
    writeBigFontDigit(x, y, timeinfo.tm_hour / 10);
    writeBigFontDigit(x + 3, y, timeinfo.tm_hour % 10);
    hd44780_gotoxy(&lcd, x + 6, y + 1);
    hd44780_putc(&lcd, (timeinfo.tm_sec & 0x01) ? ':' : ' ');
    writeBigFontDigit(x + 7, y, timeinfo.tm_min / 10);
    writeBigFontDigit(x + 10, y, timeinfo.tm_min % 10);
}

static char* monthText[12] =
{
    "Jan.", "Fev.", "Mars", "Avril", "Mai", "Juin",
    "Juil.", "Aout", "Sept.", "Oct.", "Nov.", "Dec."
};

static char* dowText[7] =
{
     "Diman.", "Lundi", "Mardi", "Mercr.", "Jeudi", "Vendr.", "Samedi"
};

static void display_date_text(int y, time_t *time)
{
    char date_buf[21];

    struct tm timeinfo;
    localtime_r(time, &timeinfo);

    if(data_getDataStruct()->dataValid)
        sprintf(date_buf, "%s %d %s %d", dowText[timeinfo.tm_wday], timeinfo.tm_mday,
                monthText[timeinfo.tm_mon], timeinfo.tm_year + 1900);
    else
        sprintf(date_buf, "Invalid data");

    int margins = sizeof(date_buf) - 1 - strlen(date_buf);
    int i = 0;

    hd44780_gotoxy(&lcd, 0, y);

    while(i < margins / 2)
    {
        hd44780_putc(&lcd, ' ');
        i++;
    }
    hd44780_puts(&lcd, date_buf);
    while(i < margins)
    {
        hd44780_putc(&lcd, ' ');
        i++;
    }
}

static void display_out_temp(int x, int y)
{
    char buf[4];

    hd44780_gotoxy(&lcd, x, y);
    hd44780_puts(&lcd, "OUT");

    int temp = data_getDataStruct()->temp;

    hd44780_gotoxy(&lcd, x, y + 1);

    if(!data_getDataStruct()->dataValid || temp <= -100 || temp >= 100)
        strcpy(buf, "err");
    else
        sprintf(buf, "%d", temp);

    // Right align by inserting "space" char before
    for(int i = strlen(buf) ; i < sizeof(buf) - 1 ; i++)
        hd44780_putc(&lcd, ' ');
    hd44780_puts(&lcd, buf);

    hd44780_gotoxy(&lcd, x, y + 2);
    hd44780_puts(&lcd, " \xDF" "C");
}

static void display_in_temp(int x, int y, int temp)
{
    char buf[4];

    hd44780_gotoxy(&lcd, x, y);
    hd44780_puts(&lcd, "IN");

    hd44780_gotoxy(&lcd, x, y + 1);

    if((temp < -55) || (temp > 125))
        strcpy(buf, "err");
    else
        sprintf(buf, "%d", temp);

    hd44780_puts(&lcd, buf);
    for(int i = strlen(buf) ; i < sizeof(buf) - 1 ; i++)
        hd44780_putc(&lcd, ' ');

    hd44780_gotoxy(&lcd, x, y + 2);
    hd44780_puts(&lcd, "\xDF" "C");
}

static void display_weather_text(int y)
{
    char weather_buf[21];
    if(data_getDataStruct()->dataValid)
        sprintf(weather_buf, "%s", data_getDataStruct()->mainWeatherText);
    else
        sprintf(weather_buf, "Invalid data");

    int margins = sizeof(weather_buf) - 1 - strlen(weather_buf);
    int i = 0;

    hd44780_gotoxy(&lcd, 0, y);

    while(i < margins / 2)
    {
        hd44780_putc(&lcd, ' ');
        i++;
    }
    hd44780_puts(&lcd, weather_buf);
    while(i < margins)
    {
        hd44780_putc(&lcd, ' ');
        i++;
    }
}

void display_task(void)
{
    time_t timeNow = 0, timeDisplayed = 0, timeTempSensor = 0;
    int tempSensorValue = TEMPSENSOR_ERROR;

    hd44780_init(&lcd);
    for(int i = 0 ; i < BIG_FONT_CHAR_COUNT ; i++)
    {
        hd44780_upload_character(&lcd, i, bigFontCustomChars[i]);
    }

    while(1)
    {
        time(&timeNow);
        if(timeNow != timeDisplayed && data_takeMutex(0))
        {
            timeDisplayed = timeNow;

            //Using weather data to get time offset
            timeNow += data_getDataStruct()->timeOffset;

            display_large_clock(3, 0, &timeNow);
            display_date_text(3, &timeNow);
            //display_out_temp(17, 0);
            //display_weather_text(3);

            data_giveMutex();
        }

//        if(timeNow != timeTempSensor && tempsensor_get_temp(0, &tempSensorValue))
//        {
//            display_in_temp(0, 0, tempSensorValue);
//            timeTempSensor = timeNow;
//        }

        vTaskDelay(50 / portTICK_PERIOD_MS);
    }
}
