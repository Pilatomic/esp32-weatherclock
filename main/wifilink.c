/* HTTPS GET Example using plain mbedTLS sockets
 *
 * Contacts the howsmyssl.com API via TLS v1.2 and reads a JSON
 * response.
 *
 * Adapted from the ssl_client1 example in mbedtls.
 *
 * Original Copyright (C) 2006-2016, ARM Limited, All Rights Reserved, Apache 2.0 License.
 * Additions Copyright (C) Copyright 2015-2016 Espressif Systems (Shanghai) PTE LTD, Apache 2.0 License.
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_system.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"
#include "esp_sntp.h"
#include "esp_tls.h"
#include "esp_crt_bundle.h"

#include "config.h"
#include "json_data.h"

/* Constants that aren't configurable in menuconfig */
#define OPENWEATHERMAP_SERVER  "api.openweathermap.org"
#define OPENWEATHERMAP_URL     "https://api.openweathermap.org/data/2.5/weather?"

static const char *TAG = "WIFILINK";

static const char *REQUEST = "GET " OPENWEATHERMAP_URL""OPENWEATHERMAP_API_PARAMS " HTTP/1.0\r\n"
    "Host: "OPENWEATHERMAP_SERVER"\r\n"
    "User-Agent: esp-idf/1.0 esp32\r\n"
    "\r\n";

#define WIFI_CONNECTED_BIT          BIT0
#define WIFI_FAIL_BIT               BIT1

/* FreeRTOS event group to signal when we are connected*/
static EventGroupHandle_t s_wifi_event_group;
static esp_event_handler_instance_t instance_any_id;
static esp_event_handler_instance_t instance_got_ip;

static void event_handler(void* arg, esp_event_base_t event_base,
                                int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START)
    {
        esp_wifi_connect();

    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED)
    {
        xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);
    }
    else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP)
    {
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    }
}

bool wifilink_waitForWifiConnection()
{
    int retry = 0;
    while(1)
    {
        EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
                                               WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
                                               pdTRUE, //Clear bits
                                               pdFALSE,
                                               portMAX_DELAY);

        if (bits & WIFI_CONNECTED_BIT)
        {
            ESP_LOGI(TAG, "connected to ap SSID:%s password:%s",
                     WIFI_SSID, WIFI_PASS);
            return true;
        }
        else if (bits & WIFI_FAIL_BIT)
        {
            ESP_LOGI(TAG, "Failed to connect to SSID:%s, password:%s",
                     WIFI_SSID, WIFI_PASS);
            if(retry < WIFI_MAXIMUM_RETRY)
            {
                retry++;
                esp_wifi_connect();
            }
            else return false;
        }
        else
        {
            ESP_LOGE(TAG, "UNEXPECTED EVENT");
            return false;
        }
    }
}

static void wifilink_setupWifi()
{
    ESP_ERROR_CHECK(esp_netif_init());

    esp_netif_create_default_wifi_sta();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
}

static void wifilink_createEvents()
{


    s_wifi_event_group = xEventGroupCreate();



    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &event_handler,
                                                        NULL,
                                                        &instance_any_id));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                                                        IP_EVENT_STA_GOT_IP,
                                                        &event_handler,
                                                        NULL,
                                                        &instance_got_ip));
}

static void wifilink_destroyEvents()
{
    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP, instance_got_ip));
    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(WIFI_EVENT, ESP_EVENT_ANY_ID, instance_any_id));
    vEventGroupDelete(s_wifi_event_group);
}

static void wifilink_beginWifiConnection()
{
    wifi_config_t wifi_config = {
        .sta = {
            .ssid = WIFI_SSID,
            .password = WIFI_PASS,
            .pmf_cfg = {
                .capable = true,
                .required = false
            },
        },
    };

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
    ESP_ERROR_CHECK(esp_wifi_start() );
}

static int onlyKeepLastLineInBuffer(char* buf, int len)
{
    char* end = buf + len;
    char* lastEolPos = 0;

    for(char* c = buf ; c < end ; c++)
    {
        if(*c == '\n') lastEolPos = c;
    }

    if(lastEolPos)
    {
        lastEolPos += 1; // Do not include line return char
        len = end - lastEolPos;
        memcpy(buf, lastEolPos, len);
    }

    return len;
}

static int wifilink_openweathermap_perform_https_transaction(char* buf, int bufLen)
{
    int ret, len, idx = 0;

    esp_tls_cfg_t cfg = {
        .crt_bundle_attach = esp_crt_bundle_attach,
    };

    struct esp_tls *tls = esp_tls_conn_http_new(OPENWEATHERMAP_URL, &cfg);

    if(tls != NULL) {
        ESP_LOGI(TAG, "Connection established...");
    } else {
        ESP_LOGE(TAG, "Connection failed...");
        goto exit;
    }

    size_t written_bytes = 0;
    do {
        ret = esp_tls_conn_write(tls,
                                 REQUEST + written_bytes,
                                 strlen(REQUEST) - written_bytes);
        if (ret >= 0) {
            ESP_LOGI(TAG, "%d bytes written", ret);
            written_bytes += ret;
        } else if (ret != ESP_TLS_ERR_SSL_WANT_READ  && ret != ESP_TLS_ERR_SSL_WANT_WRITE) {
            ESP_LOGE(TAG, "esp_tls_conn_write  returned 0x%x", ret);
            goto exit;
        }
    } while(written_bytes < strlen(REQUEST));

    ESP_LOGI(TAG, "Reading HTTP response...");

    do
    {
        len = bufLen - idx;
        bzero(buf + idx, len);
        ret = esp_tls_conn_read(tls, buf + idx, len - 1);

        if(ret == ESP_TLS_ERR_SSL_WANT_WRITE  || ret == ESP_TLS_ERR_SSL_WANT_READ)
            continue;

        if(ret < 0)
        {
            ESP_LOGE(TAG, "esp_tls_conn_read  returned -0x%x", -ret);
            idx = 0;
            break;
        }



        if(ret == 0)
        {
            ESP_LOGI(TAG, "Connection closed, %d payload bytes read", idx);

//            for(int i = 0; i < idx - 1; i++) {
//                putchar(buf[i]);
//            }
//            putchar('\n');

            break;
        }

        ESP_LOGD(TAG, "%d bytes read", ret);

        ESP_LOGD(TAG, "Idx before %d, ret %d", idx, ret);

        idx = onlyKeepLastLineInBuffer(buf, idx + ret) ;

        ESP_LOGD(TAG, "Idx after %d", idx);

    } while(1);

exit:
    esp_tls_conn_delete(tls);
    return idx;
}

static void wifilink_getWeather()
{
    char buf[1024];

    int len = wifilink_openweathermap_perform_https_transaction(buf, sizeof(buf));
    if(len > 0)
    {
        if(data_parseBuffer(buf, len))
            ESP_LOGI(TAG, "cod: %d, temp: %f", data_getDataStruct()->cod, data_getDataStruct()->temp);
        else
        {
            data_getDataStruct()->cod = 0;
            ESP_LOGW(TAG, "Failed json parsing");
        }
    }
}

static void wifilink_getTime()
{
    ESP_LOGI(TAG, "Begin SNTP time sync");

    sntp_setoperatingmode(SNTP_OPMODE_POLL);
    sntp_setservername(0, NTP_SERVER_NAME);
    sntp_set_sync_mode(SNTP_SYNC_MODE_IMMED);
    sntp_init();

    sntp_sync_status_t sntp_status = SNTP_SYNC_STATUS_RESET;
    for(int i = NTP_SYNC_MAX_DURATION_S ;
        i > 0 && sntp_status == SNTP_SYNC_STATUS_RESET ;
        i--)
    {
        ESP_LOGI(TAG, "Waiting for SNTP sync, %d s left before aborting", i);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        sntp_status = sntp_get_sync_status();
    }

    if(sntp_status == SNTP_SYNC_STATUS_RESET)
        ESP_LOGW(TAG, "SNTP sync failed");
    else
        ESP_LOGI(TAG, "SNTP sync success");

    sntp_stop();
}

static void wifilink_waitBeforeNextConnection()
{
    for(int minutesUntilRetry = REQUEST_INTERVAL_MINUTES ;
        minutesUntilRetry > 0 ;
        minutesUntilRetry--)
    {
        ESP_LOGI(TAG, "Next connection in %i minutes", minutesUntilRetry);
        vTaskDelay(60000 / portTICK_PERIOD_MS );
    }
}



void wifilink_task(void)
{
    wifilink_setupWifi();
    wifilink_createEvents();

    // Add some way to kill the loop or force refresh now ?
    while(1)
    {
        wifilink_beginWifiConnection();
        if(wifilink_waitForWifiConnection())
        {
            wifilink_getTime();
            wifilink_getWeather();
        }

        esp_wifi_stop();
        wifilink_waitBeforeNextConnection();
    }

    wifilink_destroyEvents();
}
