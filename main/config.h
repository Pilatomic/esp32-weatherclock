#ifndef WIFI_CONFIG_H
#define WIFI_CONFIG_H

#define WIFI_SSID                   "WIFI_SSID"

#define WIFI_PASS                   "WIFI_PASSWORD"

#define OPENWEATHERMAP_API_PARAMS   "q=YOUR_TOWN&lang=en&units=metric&appid=OPENWEATHERMAP_API_KEY"

#define NTP_SERVER_NAME             "pool.ntp.org"

#define NTP_SYNC_MAX_DURATION_S     10

#define WIFI_MAXIMUM_RETRY          2

#define REQUEST_INTERVAL_MINUTES    30

#endif
