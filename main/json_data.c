#include "json_data.h"
#include "mjson.h"

#include "freertos/semphr.h"


static DataStruct_t dataStruct = {0};
static SemaphoreHandle_t dataMutex = NULL;

#define MAX_WEATHERS    1

char s[32];

static const struct json_attr_t attrs_coord[] = {
    { "lon"         , t_ignore      ,   .addr.integer = NULL                            },
    { "lat"         , t_ignore      ,   .addr.integer = NULL                            },
    {NULL},
};

static const struct json_attr_t attrs_weather[] = {
    { "id"          , t_ignore      ,   .addr.integer = NULL                            },
    { "main"        , t_string      ,   .addr.string = dataStruct.mainWeatherText,
                                        .len = WEATHER_MAIN_MAX_LEN                     },
    { "description" , t_string      ,   .addr.string = dataStruct.descriptionWeatherText,
                                        .len = WEATHER_DESC_MAX_LEN                     },
    { "icon"        , t_ignore      ,   .addr.integer = NULL                            },
    {NULL},
};

static const struct json_attr_t attrs_main[] = {
    { "temp"        , t_real        ,   .addr.real = &dataStruct.temp                   },
    { "feels_like"  , t_ignore      ,   .addr.integer = NULL                            },
    { "temp_min"    , t_ignore      ,   .addr.integer = NULL                            },
    { "temp_max"    , t_ignore      ,   .addr.integer = NULL                            },
    { "pressure"    , t_ignore      ,   .addr.integer = NULL                            },
    { "humidity"    , t_ignore      ,   .addr.integer = NULL                            },
    {NULL},
};

static const struct json_attr_t attrs_wind[] = {
    { "speed"       , t_ignore      ,   .addr.integer = NULL                            },
    { "deg"         , t_ignore      ,   .addr.integer = NULL                            },
    {NULL},
};

static const struct json_attr_t attrs_clouds[] = {
    { "all"         , t_ignore      ,   .addr.integer = NULL                            },
    {NULL},
};

static const struct json_attr_t attrs_sys[] = {
    { "type"        , t_ignore      ,   .addr.integer = NULL                            },
    { "id"          , t_ignore      ,   .addr.integer = NULL                            },
    { "country"     , t_ignore      ,   .addr.integer = NULL                            },
    { "sunrise"     , t_ignore      ,   .addr.integer = NULL                            },
    { "sunset"      , t_ignore      ,   .addr.integer = NULL                            },
    {NULL},
};


static const struct json_attr_t attrs_omw[] = {
    { "coord"       , t_object      ,   .addr.attrs = attrs_coord                      },
    { "weather"     , t_array       ,   .addr.array.element_type = t_object,
                                        .addr.array.arr.objects.subtype = attrs_weather,
                                        .addr.array.maxlen = MAX_WEATHERS,
                                        .addr.array.count = &dataStruct.weathersCount   },
    { "base"        , t_ignore      ,   .addr.integer = NULL                            },
    { "main"        , t_object      ,   .addr.attrs = attrs_main                        },
    { "visibility"  , t_ignore      ,   .addr.integer = NULL                            },
    { "wind"        , t_object      ,   .addr.attrs = attrs_wind                        },
    { "clouds"      , t_object      ,   .addr.attrs = attrs_clouds                      },
    { "dt"          , t_integer     ,   .addr.integer = &dataStruct.dt                  },
    { "sys"         , t_object      ,   .addr.attrs = attrs_sys                         },
    { "timezone"    , t_integer     ,   .addr.integer = &dataStruct.timeOffset          },
    { "id"          , t_ignore      ,   .addr.integer = NULL                            },
    { "name"        , t_ignore      ,   .addr.integer = NULL                            },
    { "cod"         , t_integer     ,   .addr.integer = &dataStruct.cod                 },
    {NULL},
};



int data_parseBuffer(char* ptr, int len)
{
    int status = -1;
#ifdef DEBUG_ENABLE
    json_enable_debug(1, stdout);
#endif

    if(data_takeMutex(5000 / portTICK_PERIOD_MS))
    {
        status = json_read_object(ptr, attrs_omw, NULL);
        dataStruct.dataValid = (status == 0 && dataStruct.cod == 200);
        data_giveMutex();

        if (status != 0)
                puts(json_error_string(status));
    }
    return (status == 0);
}

DataStruct_t *data_getDataStruct()
{
    return &dataStruct;
}

int data_takeMutex(TickType_t ticksToWait)
{
    return xSemaphoreTake(dataMutex, ticksToWait);
}

int data_giveMutex()
{
    return xSemaphoreGive(dataMutex);
}

void data_init()
{
    dataMutex = xSemaphoreCreateMutex();
}
