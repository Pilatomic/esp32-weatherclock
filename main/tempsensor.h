#ifndef TEMPSENSOR_H
#define TEMPSENSOR_H

#include "freertos/FreeRTOS.h"

#define TEMPSENSOR_ERROR                  0x7FFFFFFF


void tempsensor_task();

int tempsensor_get_temp(TickType_t ticksToWait, int *temp);

#endif
